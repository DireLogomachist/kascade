//Contains all function queries required by Kascade
//A single function is paired with a feature or content display


//RETRIEVAL FUNCTIONS///////////////////////////////////////////////////////////

//Returns a Group with the matching groupID
Parse.Cloud.define("getGroup", function(request, response) {
	var query = new Parse.Query("Group");
	query.get(request.params.groupID, {
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Get Group failed");
		}
	})
});

//Returns a Group with the matching name
Parse.Cloud.define("getGroupByName", function(request, response) {
  	var query = new Parse.Query("Group");
  	query.equalTo("name", request.params.groupName)
	query.first({
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Get Group by Name failed");
		}
	})
});

//Returns the names of all Group instances
Parse.Cloud.define("getAllGroupNames", function(request, response) {
  var query = new Parse.Query("Group");
	query.select('name');
	query.find({
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Return all group names failed");
		}
	})
});

//Returns all Events hosted by the specified group via groupID
Parse.Cloud.define("getEventsByGroup", function(request, response) {
	var query = new Parse.Query("Event");
	query.ascending("date");
	query.equalTo("group", {__type: "Pointer", className: "Group", objectId: request.params.groupID});
	query.find({
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Get Events by Group failed")
		}
	})
});

//Returns the number of events hosted by a group with the matching groupID
Parse.Cloud.define("getNumberGroupEvents", function(request, response) {
	var Event = Parse.Object.extend("Event");
	var query = new Parse.Query(Event);
	query.equalTo("group", {__type: "Pointer", className: "Group", objectId: request.params.groupID});
	query.find({
		success: function(results) {
			response.success(results.length);
		},
		error: function() {
			response.error("Get Number of Group Events failed");
		}
	})
});

//Returns the number of subscribers of the group with the matching groupID
Parse.Cloud.define("getNumberSubscribers", function(request, response){
	var Group = Parse.Object.extend("Group")
	var query = new Parse.Query(Group);
	query.get(request.params.groupID, {
		success: function(results) {
			response.success(results.get('subscribers'));
		},
		error: function() {
			response.error("Get Number of Subscribers failed")
		}
	})
});

//Returns the event with the matching eventID
Parse.Cloud.define("getEvent", function(request, response){
	var Event = Parse.Object.extend("Event");
	var query = new Parse.Query(Event);
	query.get(request.params.eventID, {
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Get Event failed");
		}
	})
});

//Returns all events on the given day
Parse.Cloud.define("getEventsOnDay", function(request, response){
	var date = new Date();
	date = request.params.date;

	var nextDate = new Date();
	nextDate.setTime(date.getTime());
	var n = nextDate.getTime() + 86400000;
	nextDate.setTime(n);

	var query = new Parse.Query("Event");
	query.ascending("date");
	query.greaterThanOrEqualTo("date", date);
	query.lessThan("date", nextDate);

	query.find({
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Get Events on Day failed");
		}
	})
});
/*
//Returns all events between the two given days
Parse.Cloud.define("getEventsUptoDate", function(request, response){
	var date = new Date();
	date = request.params.prevDate;
	var nextDate = new Date();
	nextDate = request.params.nextDate;

	var query = new Parse.Query("Event");
	query.ascending("date");
	query.greaterThanOrEqualTo("date", date);
	query.lessThan("date", nextDate);

	query.find({
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Get Events Upto Date failed");
		}
	})
});*/

//Returns all events that the given user is subscribed to
Parse.Cloud.define("getSubscribedEvents", function(request, response){
	var subList = [];
	var sQuery = new Parse.Query("Subscribed");
	sQuery.equalTo("user", {__type: "Pointer", className: "_User", objectId: request.params.userID})
	sQuery.first({
		success: function(results) {
			subList = results.get('group'); //formerly 'groups' and worked...
		},
		error: function() {
			response.error("Get Subscribed Events failed - bad userID - " + request.params.userID);
			return;
		}
	})

	var query = new Parse.Query("Event");
	query.ascending("date");
	for(var i = 0; i < subList.length; i++) {
		query.equalTo("group", subList[i]);
	}
	query.find({
		success: function(results) {
			response.success(results);
		},
		error: function() {
			response.error("Get Subscribed Events failed");
		}
	})
});

//Returns whether the given group name is unique
Parse.Cloud.define("checkUniqueGroup", function(request, response){
	var query = new Parse.Query("Group");
	query.equalTo("name", request.params.groupName);
	query.first({
		success: function(results) {
			if(results) {
				response.error(request.params.groupName + " is not unique group");
			} else {
				response.success();
			}
		},
		error: function() {
			response.error("Check Unique Group failed for: " + request.params.groupName);
		}
	})
});

//Returns whether the given username is unique
Parse.Cloud.define("checkUniqueUsername", function(request, response){
	var query = new Parse.Query("_User");
	query.equalTo("username", request.params.userName);
	query.first({
		success: function(results) {
			if(results) {
				response.error(request.params.userName + " is not a unique username");
			} else {
				response.success();
			}
		},
		error: function() {
			response.error("Check Unique userName failed for: " + request.params.userName);
		}
	})
});

//Returns a list of the pointers of all subscribed groups - part 1 of a 2 step process
Parse.Cloud.define("getSubscribedGroupNames", function(request, response){
	var subList = [];
	var query = new Parse.Query("Subscribed");
	query.equalTo("user", {__type: "Pointer", className: "_User", objectId: request.params.userID});
	query.find({
		success: function(results) {
			for(var i = 0; i < results.length; i++) {
				subList.push(results[i].get('group'));
			}
			response.success(subList);
		},
		error: function() {
			response.error("Get Subscribed Groups Names failed for userID " + request.params.userID);
			return;
		}
	})

/* //Attempted to extract objectID from Pointer to query name - failed miserably
	var nameList = [];
	for(var i = 0; i < subList.length; i++) {
		var nquery = new Parse.Query("Group");
		nquery.get(subList[i], {
			success: function(results) {
				nameList.push(results.get('name'));
				//if(i == subList.length - 1) {
				//	response.success(nameList);
				//}
			},
			error: function() {
				response.error("Get Subscribed Group Names failed for OH GOD AN ALIEN AAAAAAAAAAAAAAAAA");
			}
		})
	}
*/
});

//Returns a list of the pointers of all managed groups - part 1 of a 2 step process
Parse.Cloud.define("getManagedGroupNames", function(request, response){
	var subList = [];
	var query = new Parse.Query("Admins");
	query.equalTo("userID", {__type: "Pointer", className: "_User", objectId: request.params.userID});
	query.find({
		success: function(results) {
			for(var i = 0; i < results.length; i++) {
				subList.push(results[i].get('group'));
			}
			response.success(subList);
		},
		error: function() {
			response.error("Get Managed Groups Names failed for userID " + request.params.userID);
			return;
		}
	})
});

//Returns the name of a group with the given objectID
Parse.Cloud.define("getGroupName", function(request, response){
	var query = new Parse.Query("Group");
	query.get(request.params.groupID, {
		success: function(results) {
			response.success(results.get('name'));
		},
		error: function() {
			response.error("Get Group Name failed");
		}
	})
});

//Returns a list of upcoming events, up to 20 at a time
Parse.Cloud.define("getPublicEventsBatch", function(request, response){
	var date = new Date();
	date = request.params.dateIndex;

	//Query all (public) events
	var query = new Parse.Query("Event");
	query.ascending("date");
	query.limit(60)
	query.greaterThan("date", date);

	query.find({
		success: function(results) {
			if(results.length == 0) {
				response.success("Empty");
				return;
			}
			if(results.length <= 12) {
				response.success(results);
			} else {

				var resultsList = [];
				var i;
				for(i = 0; i < 12; i++) {
					resultsList.push(results[i]);
				}
				while((i < results.length) && (resultsList[11].get('date') == results[i].get('date'))){
					resultsList.push(results[i]);
					i++;
				}

				response.success(resultsList);
				//response.success(results);
			}
		},
		error: function() {
			response.error("Get Public Events Batch failed");
		}
	})
});

//Returns a list of upcoming subscribed events, up to 20 at a time
Parse.Cloud.define("getSubscribedEventsBatch", function(request, response){
	var date = new Date();
	date = request.params.dateIndex;

	//List of all subbed groups
	var subList = [];
	var sQuery = new Parse.Query("Subscribed");
	sQuery.equalTo("user", {__type: "Pointer", className: "_User", objectId: request.params.userID})
	
	//Query events from subbed groups
	var query = new Parse.Query("Event");
	query.ascending("date");
	query.limit(60);
	query.greaterThan("date", date);

	sQuery.find({
		success: function(results) {
			if(results.length == 0) {
				response.success("Empty");
				return;
			}

			for(var i = 0; i < results.length; i++) {
				subList.push(results[i].get('group'));
			}

			for(var i = 0; i < subList.length; i++) {
				query.equalTo("group", subList[i]);
			}
			query.find({
				success: function(results) {
					if(results.length == 0) {
						response.success("Empty");
						return;
					}

					if(results.length <= 12) {
						response.success(results);
					} else {

						var resultsList = [];
						var i;
						for(i = 0; i < 12; i++) {
							resultsList.push(results[i]);
						}
						while((i < results.length) && (resultsList[11].get('date') == results[i].get('date'))){
							resultsList.push(results[i]);
							i++;
						}

						response.success(resultsList);
					}

				},
				error: function() {
					response.error("Get Subscribed Events Batch failed");
					return;
				}
			})
		},
		error: function() {
			response.error("Get Subscribed Events Batch failed - public - userID - " + request.params.userID);
		}
	})
});

Parse.Cloud.job("reccurrenceScheduler", function(request, status) {
	Parse.Cloud.useMasterKey();

	var date = new Date();
	var query = new Parse.Query("Event");
	query.lessThan("date", date);
	query.each(function(event) {

		var endDate = event.get('endDate');
		if(event.get('recurring') == true && endDate <= date) {
			event.set("recurring", false);
			event.save();
		}

		if(event.get('recurring') == true && endDate > date) {
			//day.getDay returns 0 - 6 for the day of the week
			//recurringDays array - 6 entries - 0 if not repeating - 1 if so

			var check = false;
			var dayIncrement = 0;
			var start = event.get('date').getDay() + 1;
			if(start == 7) {start = 0;}
			for(var i = start; i < 7; i++) {
				if(event.get('recurringDays')[i] == 1) { 
					dayIncrement = i;
					check = true;
					break;
				}
			}
			if(!check) {
				for(var i = 0; i < start; i++) {
					if(event.get('recurringDays')[i] == 1) {
						dayIncrement = i;
						check = true;
						break;
					}
				}
			}
			if(check) {
				var offset = dayIncrement - event.get('date').getDay();
				if(offset < 0) { offset = 7 + offset; }
				if(offset == 0) { offset = 7; }
				event.set("date", new Date(event.get('date').getTime() + (offset * 86400000)));
				event.save();
			} else {
				//event.destroy({});
			}
		} else {
			//event.destroy({});
		}
	}).then(function() {
		status.success("Scheduling pass completed");
	}, function(error) {
		status.error("Scheduling pass failed");
	});
});

Parse.Cloud.define("getPublicEventsOnDay", function(request, response) {
	var date = new Date();
	var nextDate = new Date();
	date = request.params.date; //Assumes that date starts at 00:00 - month-day-year only
	nextDate.setTime(date.getTime() + 86400000);

	//Query all (public) events
	var query = new Parse.Query("Event");
	query.ascending("date");
	query.greaterThanOrEqualTo("date", date);
	query.lessThan("date", nextDate);

	query.find({
		success: function(results) {
			if(results.length == 0) {
				response.success("Empty");
				return;
			}

			response.success(results);
		},
		error: function() {
			response.error("Get Public Events On Day failed");
		}
	});
});


Parse.Cloud.define("getSubscribedEventsOnDay", function(request, response) {
	var date = new Date();
	var nextDate = new Date();
	date = request.params.date; //Assumes that date starts at 00:00 - month-day-year only
	nextDate.setTime(date.getTime() + 86400000);

	//List of all subbed groups
	var subList = [];
	var sQuery = new Parse.Query("Subscribed");
	sQuery.equalTo("user", {__type: "Pointer", className: "_User", objectId: request.params.userID})
	
	//Query events from subbed groups
	var query = new Parse.Query("Event");
	query.ascending("date");
	query.greaterThanOrEqualTo("date", date);
	query.lessThan("date", nextDate);

	sQuery.find({
		success: function(results) {
			if(results.length == 0) {
				response.success("Empty");
				return;
			}

			for(var i = 0; i < results.length; i++) {
				subList.push(results[i].get('group'));
			}

			for(var i = 0; i < subList.length; i++) {
				query.equalTo("group", subList[i]);
			}
			query.find({
				success: function(results) {
					response.success(results);
				},
				error: function() {
					response.error("Get Subscribed Events On Day failed");
					return;
				}
			});
		},
		error: function() {
			response.error("Get Subscribed Events On Day failed - userID - " + request.params.userID);
		}
	});
});


Parse.Cloud.define("getPublicEventNumbersOnMonth", function(request, response) {
	var month = request.params.month;
	var year = request.params.year;
	var nextMonth = request.params.month + 1;
	var nextYear = year;
	if(nextMonth > 11) {
		nextYear = year + 1;
		nextMonth = 0;
	} else {
		nextYear = year;
	}

	var startDate = new Date();
	startDate.setUTCHours(0,0,0,0);
	startDate.setFullYear(year, month, 1);
	var endDate = new Date();
	endDate.setUTCHours(0,0,0,0);
	endDate.setFullYear(nextYear, nextMonth, 1);

	var query = new Parse.Query("Event");
	query.ascending("date");
	query.greaterThanOrEqualTo("date", startDate);
	query.lessThan("date", endDate);

	//returns list of all events in month
	query.find({
		success: function(results) {
			//iterate through days in month and if exists one or more events on day, enter 0 or 1 in array
			var resultsList = [];
			for(var i = 0; i < new Date(nextMonth, nextMonth, 0).getDate(); i++) {
				resultsList.push(0)
			}

			for(var i = 0; i < results.length; i++) {
				resultsList[results[i].get('date').getDate()] = 1;
			}

			response.success(resultsList);
		},
		error: function() {
			response.error("Get Public Event Numbers On Month failed");
		}
	});
});

Parse.Cloud.define("getSubscribedEventNumbersOnMonth", function(request, response) {
	var month = request.params.month;
	var year = request.params.year;
	var nextMonth = request.params.month + 1;
	var nextYear = year;
	if(nextMonth > 11) {
		nextYear = year + 1;
		nextMonth = 0;
	} else {
		nextYear = year;
	}

	var startDate = new Date();
	startDate.setUTCHours(0,0,0,0);
	startDate.setFullYear(year, month, 1);
	var endDate = new Date();
	endDate.setUTCHours(0,0,0,0);
	endDate.setFullYear(nextYear, nextMonth, 1);


	var subList = [];
	var sQuery = new Parse.Query("Subscribed");
	sQuery.equalTo("user", {__type: "Pointer", className: "_User", objectId: request.params.userID})

	var query = new Parse.Query("Event");
	query.ascending("date");
	query.greaterThanOrEqualTo("date", startDate);
	query.lessThan("date", endDate);

	//returns list of all subscribed groups, then queries for all subscribed events
	sQuery.find({
		success: function(results) {
			var resultsList = [];
			for(var i = 0; i < new Date(nextMonth, nextMonth, 0).getDate(); i++) {
				resultsList.push(0)
			}

			if(results.length == 0) {
				response.success(resultsList);
				return;
			}

			for(var i = 0; i < results.length; i++) {
				subList.push(results[i].get('group'));
			}

			for(var i = 0; i < subList.length; i++) {
				query.equalTo("group", subList[i]);
			}

			query.find({
				success: function(results) {
					//iterate through days in month and if exists one or more events on day, enter 0 or 1 in array

					for(var i = 0; i < results.length; i++) {
						resultsList[results[i].get('date').getDate()] = 1;
					}

					response.success(resultsList);
				},
				error: function() {
					response.error("Get Subscribed Event Numbers On Month failed");
				}
			});
		},
		error: function() {
			response.error("Get Subscribed Event Numbers On Month failed - userID - " + request.params.userID);
		}
	});
});

//Returns whether the given user is subscribed to the given group
Parse.Cloud.define("checkIfSubscribed", function(request, response){
	var query = new Parse.Query("Subscribed");
	query.equalTo("user", {__type: "Pointer", className: "_User", objectId: request.params.userID});
	query.equalTo("group", {__type: "Pointer", className: "Group", objectId: request.params.groupID});
	query.find({
		success: function(results) {
			if (results.length == 0) {
				response.success("false");
			} else {
				response.success("true");
			}
		},
		error: function() {
			response.error("Check If Subscribed failed for: " + request.params.userID);
		}
	});
});










