//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastNameCopyright (c) 2015 DireLogomachist. All rights reserved.
//

import UIKit

class SubscribedTo: UITableViewController {

    var groupNameToPass : String!
    var groupIDToPass : String!
    var path = NSIndexPath()
    struct group {
        var name : String = ""
        var id : String = ""
    }
    var subscribed : [group] = [group]()
    var gr : group! = group()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController?.navigationBar.topItem?.title = "Subscribed To"

        if Reachability.isConnectedToNetwork() {
            loadParseData()
        }
        else {
            let alert = UIAlertController(title:"", message:"We can't find the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func loadParseData() {
        PFCloud.callFunctionInBackground("getSubscribedGroupNames", withParameters: ["userID" : PFUser.currentUser().objectId]){(response: AnyObject!, error: NSError!) -> Void in
            
            if response.count == 0 {
                let alert = UIAlertController(title:"", message:"You are not subscribed to any groups", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Continue", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            
            if response != nil && error == nil {
                for i in 0...(response.count-1) {
                    self.gr.id = (String)(response[i]!.objectId as String)
                    self.subscribed.append(self.gr)
                }
                self.load2()
            } else {
                let alert = UIAlertController(title:"", message:"There was an error getting groups.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func load2() {
        for j in 0..<subscribed.count {
            PFCloud.callFunctionInBackground("getGroupName", withParameters: ["groupID" : subscribed[j].id])
                {(response: AnyObject!, error: NSError!) -> Void in
                if response != nil && error == nil {
                    self.subscribed[j].name = response.description as String
                    self.tableView.reloadData()
                } else {
                    let alert = UIAlertController(title:"", message:"There was an error getting groups.", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return subscribed.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("subscribedToCell", forIndexPath: indexPath) as UITableViewCell

        cell.textLabel?.text = subscribed[indexPath.row].name

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        groupNameToPass = subscribed[path.row].name
        groupIDToPass = subscribed[path.row].id
        performSegueWithIdentifier("subscribedToGroup", sender: self)
        tableView.deselectRowAtIndexPath(path, animated: true)
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        path = indexPath
        return indexPath
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "subscribedToGroup"){
            let temp = segue.destinationViewController as GroupPage
            temp.passedGroupName = groupNameToPass
            temp.passedGroupID = groupIDToPass
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
