//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/15/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class ManagedGroups: UITableViewController {

    var groupNameToPass : String!
    var groupIDToPass : String!
    var path = NSIndexPath()
    
    struct group {
        var name : String = ""
        var id : String = ""
    }
    var managed : [group] = [group]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        if Reachability.isConnectedToNetwork() {
            loadParseData()
        }
        else {
            let alert = UIAlertController(title:"", message:"We can't find the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func loadParseData() {
        PFCloud.callFunctionInBackground("getManagedGroupNames", withParameters: ["userID" : PFUser.currentUser().objectId]){(response: AnyObject!, error: NSError!) -> Void in
            if response.count == 0 {
                let alert = UIAlertController(title:"", message:"You do not manage any groups.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Continue", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            if response != nil && error == nil {
                for i in 0...(response.count-1) {
                    var gr : group! = group()
                    gr.id = (String)(response[i]!.objectId as String)
                    self.managed.append(gr)
                }
                self.load2()
            } else {
                let alert = UIAlertController(title:"", message:"There was an error getting groups.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func load2() {
        for j in 0..<managed.count {
            PFCloud.callFunctionInBackground("getGroupName", withParameters: ["groupID" : managed[j].id])
                {(response: AnyObject!, error: NSError!) -> Void in
                    if response != nil && error == nil {
                        self.managed[j].name = response.description as String
                        self.tableView.reloadData()
                    } else {
                        let alert = UIAlertController(title:"", message:"There was an error getting groups.", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return managed.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("managedCell", forIndexPath: indexPath) as UITableViewCell

        cell.textLabel?.text = managed[indexPath.row].name

        return cell
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        path = indexPath
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        groupNameToPass = managed[path.row].name
        groupIDToPass = managed[path.row].id
        performSegueWithIdentifier("managedToAdmin", sender: self)
        tableView.deselectRowAtIndexPath(path, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "managedToAdmin") {
            let temp = segue.destinationViewController as AdminPage
            temp.passedGroupName = groupNameToPass
            temp.passedGroupID = groupIDToPass
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
