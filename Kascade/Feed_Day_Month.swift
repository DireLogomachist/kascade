///  Kascade
//Copyright 2015 Miller and Grove City College. All rights reserved
// Mean Machine
//Miller, Andy
//4/26/15
//last data modified
// list of significant changes
//      public switch button now changes feed sources and reloads table data
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class Feed_Day_Month: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //var feedView : UITableView!
    @IBOutlet weak var eventFeed: UITableView!
    var cellID = "EventCell"
    
    var selectedDate = NSDate()//Noah - using for data passing between month -> dayView
    
    var loading = false
    var scrolling = false
    var publicSwitch = true
    var publicEndReached = false
    var subscribedEndReached = false
    var publicFeedIndex : NSDate = NSDate()
    var subscribedFeedIndex : NSDate = NSDate()
    var eventToPass : event!
    
    @IBOutlet weak var publicFeeds: UISwitch!
    
    @IBAction func publicSwitchChanged(sender: AnyObject) {
        //Reload table with correct eventList
        publicSwitch = publicFeeds.on
        //feedView.reloadData()
        eventFeed.reloadData()
        NSLog("Switching subscribed/public feeds")
    }
    
    //Testing the reloadData method instead of the insertRowsAtPath method
    struct event {
        var name : String = ""
        var group : String = ""
        var location : String = ""
        var time : String = ""
        var objectId : String = ""
    }
    
    var eventListPublic : [event] = [event]()
    var eventListSubscribed : [event] = [event]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //http://stackoverflow.com/questions/19019900/how-to-turn-off-the-automatic-gesture-to-go-back-a-view-with-a-navigation-contro
        //Method ONE to stop auto sliding capabilities of tab bar
        //self.navigationController?.interactivePopGestureRecognizer.enabled = false//not working for some reason
        
        //Method TWO to stop auto sliding capabilities of tab bar
        /*self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer!) -> Bool {
            return false;
        }*/
        
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
/*
        feedView = UITableView(frame: CGRectMake(0, self.view.frame.size.height * 0.23, self.view.frame.size.width * 0.5, self.view.frame.size.height * 0.77))
        feedView.center.x = self.view.center.x
        feedView.rowHeight = 85.0
        
        feedView.registerClass(EventFeedCell.self, forCellReuseIdentifier: cellID)
        feedView.delegate = self
        feedView.dataSource = self
        */
        //eventFeed.registerClass(EventCell.self, forCellReuseIdentifier: "FeedCell")
        eventFeed.delegate = self
        eventFeed.dataSource = self
        publicSwitch = publicFeeds.on
        
        //self.view.addSubview(feedView)
        
        if Reachability.isConnectedToNetwork() {
            let d : NSDate = NSDate()
            publicFeedIndex = d
            subscribedFeedIndex = d;
            
            loadPublicParseData()
            loadSubscribedParseData()
        }  else {
            NSLog("Connection error - Check the tubes")
            //Ethan's internet alert
            let alert = UIAlertController(title:"", message:"Internet Connection Error", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : EventCell = tableView.dequeueReusableCellWithIdentifier("feedCell", forIndexPath: indexPath) as EventCell
        
        if(publicSwitch) {
            cell.name.text = eventListPublic[indexPath.row].name
            cell.time.text = eventListPublic[indexPath.row].time
            cell.location.text = eventListPublic[indexPath.row].location
            cell.group.text = eventListPublic[indexPath.row].group
        } else {
            cell.name.text = eventListSubscribed[indexPath.row].name
            cell.time.text = eventListSubscribed[indexPath.row].time
            cell.location.text = eventListSubscribed[indexPath.row].location
            cell.group.text = eventListSubscribed[indexPath.row].group
        }
        
        cell.sizeToFit()
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!publicSwitch) {
            return eventListSubscribed.count
        }
        return eventListPublic.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(publicSwitch) {
            eventToPass = event()
            eventToPass.name = eventListPublic[indexPath.row].name
            eventToPass.time = eventListPublic[indexPath.row].time
            eventToPass.location = eventListPublic[indexPath.row].location
            eventToPass.group = eventListPublic[indexPath.row].group
            eventToPass.objectId = eventListPublic[indexPath.row].objectId
        } else {
            eventToPass = event()
            eventToPass.name = eventListSubscribed[indexPath.row].name
            eventToPass.time = eventListSubscribed[indexPath.row].time
            eventToPass.location = eventListSubscribed[indexPath.row].location
            eventToPass.group = eventListSubscribed[indexPath.row].group
            eventToPass.objectId = eventListSubscribed[indexPath.row].objectId
        }
        
        performSegueWithIdentifier("feedToDetail", sender: self)
        eventFeed.deselectRowAtIndexPath(indexPath, animated: false)
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "feedToDetail") {
            var view = segue.destinationViewController as EventDetails
            
            view.namePassed = eventToPass.name
            view.datePassed = eventToPass.time
            view.locationPassed = eventToPass.location
            view.groupPassed = eventToPass.group
            view.idPassed = eventToPass.objectId
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 40 {
            scrollUpdate()
        }
    }
    
    func setLoadingState(load:Bool) {
        self.loading = load
        //feedView.tableFooterView?.hidden = !loading
    }
    
    func loadPublicParseData() {
        if publicEndReached { return }
        //setLoadingState(true)
        
        
        var day : NSDate!
        //Call getPublicEventsBatch
        PFCloud.callFunctionInBackground("getPublicEventsBatch", withParameters: ["dateIndex" : publicFeedIndex, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
            
            if error == nil {
                //Empty response
                if response.description == "Empty" {
                    self.subscribedEndReached = true
                    //NSLog(response.description)
                    return
                }
                
                //Loop through retrieved data and add cells to tableView
                //NSLog("Public Cells - " + (String)(response.count))
                
                
                for var i = 0; i < response.count; i++ {
                    var ev : event! = event()
                    
                    //NSLog(response.description)
                    
                    ev.name = (String)(response[i]["name"] as String)
                    ev.group = (String)(response[i]["groupName"] as String)
                    ev.location = (String)(response[i]["location"] as String)
                    ev.objectId = (String)(response[i]!.objectId as String)
                    
                    var formatter = NSDateFormatter()
                    formatter.dateFormat = "h:mm a - MM/dd/yy"
                    
                    //formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                    //formatter.timeStyle = NSDateFormatterStyle.ShortStyle
                    day = response[i]["date"] as NSDate
                    
                    var date : String = formatter.stringFromDate(day)
                    
                    
                    ev.time = date
                    
                    self.eventListPublic.append(ev)
                }
                
               
                //At the end of additions reload data, set loading state off, and move date indices
                self.eventFeed.reloadData()
                self.publicFeedIndex = day
                self.setLoadingState(false)

            } else {
                //Error auto-logs
            }
        }
    }
    
    func loadSubscribedParseData() {
        if subscribedEndReached { return }
        //setLoadingState(true)
        
        var day = NSDate()
        //Call getPublicEventsBatch
        PFCloud.callFunctionInBackground("getSubscribedEventsBatch", withParameters: ["dateIndex" : subscribedFeedIndex, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                
                //Empty response
                if response.description == "Empty" {
                    self.subscribedEndReached = true
                    //NSLog(response.description)
                    return
                }
                
                //Loop through retrieved data and add cells to tableView
                //NSLog("Subbed Cells - " + (String)(response.count))
                for var i = 0; i < response.count; i++ {
                    var ev : event! = event()
                    ev.name = (String)(response[i]["name"] as String)
                    ev.group = (String)(response[i]["groupName"] as String)
                    ev.location = (String)(response[i]["location"] as String)
                    ev.objectId = (String)(response[i]!.objectId) as String
                    
                    var formatter = NSDateFormatter()
                    formatter.dateFormat = "h:mm a - MM/dd/yy"
                    //formatter.dateStyle = NSDateFormatterStyle.ShortStyle
                    //formatter.timeStyle = NSDateFormatterStyle.ShortStyle
                    
                    day = response[i]["date"] as NSDate
                    let date : String = formatter.stringFromDate(day)
                    ev.time = date
                    
                    self.eventListSubscribed.append(ev)
                }
                
                //At the end of additions reload data, set loading state off, and move date indices
                //self.feedView.reloadData()
                
                
                //sleep(1.0)
                
                self.eventFeed.reloadData()
                self.subscribedFeedIndex = day
                self.setLoadingState(false)
            } else {
                //Error auto-logs
            }
        }
    }
    
    func scrollUpdate() {
        if(!self.loading) {
            self.setLoadingState(true)
            if(Reachability.isConnectedToNetwork()) {
                if(publicSwitch) {
                    loadPublicParseData()
                } else {
                    loadSubscribedParseData()
                }
            } else {
                let alert = UIAlertController(title:"", message:"Internet Connection Error", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    

}





