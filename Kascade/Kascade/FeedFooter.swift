//  FeedFooter.swift
//  Andy Miller
//  Used for displaying a loading bar while the table loads from Parse
//  Last edited: 3/24/15

import UIKit

class FeedFooter: UIView {
    
    var activityIndicator:UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(self.frame.origin.x + 10, self.frame.origin.y - 10, 100, 100))

    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    override var hidden:Bool {
        get {
            return super.hidden
        }
        set(hidden) {
            super.hidden = hidden
//            if self.activityIndicator {
//                if hidden {
//                    self.activityIndicator.startAnimating()
//                } else {
//                    self.activityIndicator.stopAnimating()
//                }
//            }
        }
    }
    
    override func awakeFromNib() {
        self.activityIndicator.startAnimating()
    }

}
