//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class EventDetails: UIViewController {
    
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    @IBOutlet weak var eventDetails: UITextView!
    @IBOutlet weak var eventGroup: UILabel!
    
    var namePassed : String!
    var datePassed : String!
    var locationPassed : String!
    var groupPassed : String!
    var idPassed : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController?.navigationBar.topItem?.title = "Event Details"
        
        eventName.text = namePassed
        eventDate.text = datePassed
        eventLocation.text = locationPassed
        eventGroup.text = groupPassed
        
        //Only eventDetails/event description needs to be fetched from online
        getEventDescription()
        
    }

    func getEventDescription() {
        
        if (Reachability.isConnectedToNetwork()) {
            PFCloud.callFunctionInBackground("getEvent", withParameters: ["eventID" : idPassed]) {(response: AnyObject!, error: NSError!) -> Void in
                if error == nil {
                    self.eventDetails.text = response["description"] as String
                } else {
                    let alert = UIAlertController(title:"", message:"There was an error getting the description.", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
            }
        } else {
            let alert = UIAlertController(title:"", message:"Internet Connection Error", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        
        
    }
    


}
