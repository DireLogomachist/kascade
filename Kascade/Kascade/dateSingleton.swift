//
//  dateSingleton.swift
//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15

import Foundation

class dateSingleton {
    
    //Added members
    var selectedDay : CVDate?
    var segued : Bool?
    
    struct Static
    {
        static var instance: dateSingleton?
    
        static var token: dispatch_once_t = 0
    }
    
    class var sharedInstance: dateSingleton
    {
        if !(Static.instance != nil)
        {
            Static.instance = dateSingleton()
        }
        
        return Static.instance!
    }
    
}
