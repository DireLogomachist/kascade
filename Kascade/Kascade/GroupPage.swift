//
//  GroupPage.swift
//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class GroupPage: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var passedGroupName : String = ""
    var passedGroupID : String = ""
    var currentGroup = PFObject(className: "Group")!
    var subscribed : Bool!
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var subscribeButton : UIButton!
    @IBOutlet weak var subscribersLabel : UILabel!
    @IBOutlet weak var groupNameLabel : UILabel!
    
    @IBOutlet weak var groupDescLabel: UITextView!
    @IBOutlet weak var groupFeedView: UITableView!
    
    struct event {
        var name : String = ""
        var group : String = ""
        var location : String = ""
        var time : String = ""
        var objectId : String = ""
    }
    
    var eventList : [event] = [event]()
    var eventToPass : event!
    
    @IBAction func subscribeAlert() {
        if subscribed == true {
            let alert = UIAlertController(title:"Subscribe", message:"You are already subscribed to \(passedGroupName)", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            return
        }
        else {
            let alert = UIAlertController(title:"Subscribe", message:"Do you want to subscribe to \(passedGroupName)?", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: {(alertAction) -> Void in self.subscribe()}))
            alert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }

    }
    
    func subscribe() {
        var newSubscribe = PFObject(className: "Subscribed")
        
        newSubscribe["group"] = currentGroup
        newSubscribe["user"] = PFUser.currentUser()
        newSubscribe["privateAccess"] = false
        
        newSubscribe.saveInBackgroundWithBlock {
            (success: Bool, error: NSError!) -> Void in
            if success && error == nil {
                self.currentGroup.incrementKey("subscribers")
                self.currentGroup.saveInBackgroundWithBlock {
                    (success: Bool, error: NSError!) -> Void in
                    if success && error == nil {
                        self.subscribed = true
                        self.subscribeButton.setTitle("Subscribed", forState: UIControlState.Normal)
                        return
                    }
                    else {
                        let alert = UIAlertController(title:"", message: "Your subscription could not be saved.", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                }
            }
            else {
                let alert = UIAlertController(title:"", message: "Your subscription could not be saved.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    func loadParseDataByID() {
        var query = PFQuery(className: "Group")
        query.getObjectInBackgroundWithId("\(passedGroupID)") {
            (Group: PFObject!, error: NSError!) -> Void in
            if error == nil && Group != nil {
                self.currentGroup = Group
                
                PFCloud.callFunctionInBackground("checkIfSubscribed", withParameters: ["userID" : PFUser.currentUser().objectId, "groupID" : self.currentGroup.objectId]){(response: AnyObject!, error: NSError!) -> Void in
                    if (response.description == "true" && error == nil) {
                        self.subscribeButton.setTitle("Subscribed", forState: UIControlState.Normal)
                        self.subscribed = true
                        self.displayGroupData()
                    }
                    else if (response.description == "false" && error == nil) {
                        self.subscribeButton.setTitle("Subscribe", forState: UIControlState.Normal)
                        self.subscribed = false
                        self.displayGroupData()
                    }
                    else {
                        let alert = UIAlertController(title:"", message: "There was an error.", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                }
            }
            else {
                let alert = UIAlertController(title:"", message: "Could not obtain group data.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    func loadParseDataByName() {
        var query = PFQuery(className: "Group")
        query.whereKey("name", equalTo: passedGroupName)
        query.getFirstObjectInBackgroundWithBlock {
            (Group: PFObject!, error: NSError!) -> Void in
            if Group != nil && error == nil {
                self.currentGroup = Group

                PFCloud.callFunctionInBackground("checkIfSubscribed", withParameters: ["userID" : PFUser.currentUser().objectId, "groupID" : self.currentGroup.objectId]){(response: AnyObject!, error: NSError!) -> Void in
                    if (response.description == "true" && error == nil) {
                        self.subscribeButton.setTitle("Subscribed", forState: UIControlState.Normal)
                        self.subscribed = true
                        self.displayGroupData()
                    }
                    else if (response.description == "false" && error == nil) {
                        self.subscribeButton.setTitle("Subscribe", forState: UIControlState.Normal)
                        self.subscribed = false
                        self.displayGroupData()
                    }
                    else {
                        let alert = UIAlertController(title:"", message: "There was an error.", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                }
            }
            else {
                let alert = UIAlertController(title:"", message: "Could not obtain group data.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    func displayGroupData() {
        groupNameLabel.text = currentGroup["name"] as? String
        groupDescLabel.text = currentGroup["description"] as? String
        groupDescLabel.textAlignment = NSTextAlignment.Center
        
        var temp : Int!
        temp = currentGroup["subscribers"] as Int
        subscribersLabel.text = ("Subscribers: \(temp)")
        var tempImageFile : PFFile!
        var tempUIImage : UIImage!
        tempImageFile = currentGroup["headerImage"] as PFFile
        tempImageFile.getDataInBackgroundWithBlock {
            (imageData: NSData!, error: NSError!) -> Void in
            if error == nil {
                tempUIImage = UIImage(data: imageData)
                self.headerImageView.image = tempUIImage
            }
        }
        
        loadTableData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        groupFeedView.delegate = self
        groupFeedView.dataSource = self
        
        if Reachability.isConnectedToNetwork() {
            if passedGroupID != "" {
                loadParseDataByID()
            }
            else {
                loadParseDataByName()
            }
        }
        else {
            let alert = UIAlertController(title:"", message:"We can't find the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.view.backgroundColor = UIColor.whiteColor()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toGroupPage(segue: UIStoryboardSegue) {}
    
    
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : EventCell = tableView.dequeueReusableCellWithIdentifier("groupCell", forIndexPath: indexPath) as EventCell
        
        cell.name.text = eventList[indexPath.row].name
        cell.time.text = eventList[indexPath.row].time
        cell.location.text = eventList[indexPath.row].location
        cell.group.text = eventList[indexPath.row].group
        
        cell.sizeToFit()
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return eventList.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        eventToPass = event()
        eventToPass.name = eventList[indexPath.row].name
        eventToPass.time = eventList[indexPath.row].time
        eventToPass.location = eventList[indexPath.row].location
        eventToPass.group = eventList[indexPath.row].group
        eventToPass.objectId = eventList[indexPath.row].objectId
        
        performSegueWithIdentifier("groupToDetail", sender: self)
        groupFeedView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "groupToDetail" {
            let temp = segue.destinationViewController as EventDetails
            
            temp.namePassed = eventToPass.name
            temp.datePassed = eventToPass.time
            temp.locationPassed = eventToPass.location
            temp.groupPassed = eventToPass.group
            temp.idPassed = eventToPass.objectId
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
func loadTableData() {

    var day = NSDate()
    PFCloud.callFunctionInBackground("getEventsByGroup", withParameters: ["groupID" : self.currentGroup.objectId ]) {(response: AnyObject!, error: NSError!) -> Void in
        if error == nil {
            
            for var i = 0; i < response.count; i++ {
                var ev : event! = event()
                ev.name = (String)(response[i]["name"] as String)
                ev.group = (String)(response[i]["groupName"] as String)
                ev.location = (String)(response[i]["location"] as String)
                ev.objectId = (String)(response[i]!.objectId) as String
                
                var formatter = NSDateFormatter()
                formatter.dateFormat = "h:mm a - MM/dd/yy"
                day = response[i]["date"] as NSDate
                let date : String = formatter.stringFromDate(day)
                ev.time = date
                
                self.eventList.append(ev)
            }
            
            self.groupFeedView.reloadData()
        } else {
            //Error auto-logs
        }
    }
}
}
