//
//  ViewController.swift
//  CVCalendar Demo
//
//  Created by Мак-ПК (Mozharovsky)on 1/3/15.
//  Copyright (c) 2015 GameApp. All rights reserved.
//

//Code from https://github.com/Mozharovsky/CVCalendar
//Credit to author is found there as well as directions on how to use his code

import UIKit

class MonthView: UIViewController, CVCalendarViewDelegate {
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBAction func toggleSubscribedSwitch(sender: AnyObject) {
        
        if subscribedPublicFlag == 0
        {
            subscribedPublicFlag = 1
        }
        else if subscribedPublicFlag == 1
        {
            subscribedPublicFlag = 0
        }
        
        calendarView.contentController.delegate.updateFrames()
    }
    
    var subscribedPublicFlag = 0
    
    var shouldShowDaysOut = true
    var animationFinished = true
    
    var masterContainerPublic = Array<Array<Int>>()
    var masterContainerSubscribed = Array<Array<Int>>()
    
    let numOfMonths = 3//initially 3 and then we will add on more months as we traverse the calendar
    let numOfDays = 31
    
    var passedTodaysDateOnce = false //NEC temporary
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passedTodaysDateOnce = false //When a day is selected in the month view, that date will be sent via the dateSingleton to the dayView and the tabBarController will switch to dayView. This bool is needed since as soon as the monthView is viewed, it would send to current day since that day is initially selected
        
        if Reachability.isConnectedToNetwork() {
            
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: date)
            var currentYear = components.year
            var currentMonth = components.month//note currentMonth is returned 1 based...example: April = 4...parse parameters are 0 based and April would be 3
            currentMonth = currentMonth - 1//current month is now 0 based
            var calendarEntryMonth = currentMonth
            
            for p in 0 ..< 6 {
                masterContainerPublic.append(Array(count: 31, repeatedValue:-1))
            }
            for p in 0 ..< 6 {
                masterContainerSubscribed.append(Array(count: 31, repeatedValue:-1))
            }
            
            var presentMonth = [Int](count: 31, repeatedValue: -1)
            var pastMonth = [Int](count: 31, repeatedValue: -1)
            var futureMonth = [Int](count: 31, repeatedValue: -1)
            
            //Populating PUBLIC events
            //SIX nested parse calls that will populate the current month, past month, and FOUR future months with dots if that day has any events
            PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth , "year" : currentYear]) {(response: AnyObject!, error: NSError!) -> Void in
                if (response != nil && error == nil) {
                    
                    //Populate present month with 1st call
                    for(var i = 0; i < response.count; i++) {
                        presentMonth[i] = response[i] as Int
                        if response[i] as Int == 1
                        {
                            NSLog("Response Current month call index + \(i) gives 1")
                        }
                    }
                    
                    self.masterContainerPublic[1] = presentMonth
                    self.calendarView.contentController.delegate.updateFrames()
                    
                    //2nd Call to populate past month
                    PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth - 1 , "year" : currentYear]) {(response: AnyObject!, error: NSError!) -> Void in
                        if (response != nil && error == nil) {
                            
                            for(var i = 0; i < response.count; i++) {
                                pastMonth[i] = response[i] as Int
                                if response[i] as Int == 1
                                {
                                    NSLog("Response Past index + \(i) gives 1")
                                }
                            }
                            
                            self.masterContainerPublic[0] = pastMonth
                            self.calendarView.contentController.delegate.updateFrames()
                            
                            //3rd Call to populate future month
                            PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 1 , "year" : currentYear]) {(response: AnyObject!, error: NSError!) -> Void in
                                if (response != nil && error == nil) {
                                    
                                    for(var i = 0; i < response.count; i++) {
                                        futureMonth[i] = response[i] as Int
                                        if response[i] as Int == 1
                                        {
                                            NSLog("Response future index + \(i) gives 1")
                                        }
                                    }
                                    
                                    self.masterContainerPublic[2] = futureMonth
                                    self.calendarView.contentController.delegate.updateFrames()
                                    
                                    //4th Call to populate future +1 month
                                    PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 2 , "year" : currentYear]) {(response: AnyObject!, error: NSError!) -> Void in
                                        if (response != nil && error == nil) {
                                            
                                            for(var i = 0; i < response.count; i++) {
                                                futureMonth[i] = response[i] as Int
                                                if response[i] as Int == 1
                                                {
                                                    NSLog("Response future +1 index + \(i) gives 1")
                                                }
                                            }
                                            
                                            self.masterContainerPublic[3] = futureMonth
                                            self.calendarView.contentController.delegate.updateFrames()
                                            
                                            //5th Call to populate future +2 month
                                            PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 3 , "year" : currentYear]) {(response: AnyObject!, error: NSError!) -> Void in
                                                if (response != nil && error == nil) {
                                                    
                                                    for(var i = 0; i < response.count; i++) {
                                                        futureMonth[i] = response[i] as Int
                                                        if response[i] as Int == 1
                                                        {
                                                            NSLog("Response future +2 index + \(i) gives 1")
                                                        }
                                                    }
                                                    
                                                    self.masterContainerPublic[4] = futureMonth
                                                    self.calendarView.contentController.delegate.updateFrames()
                                                    
                                                    //6th Call to populate future +3 month
                                                    PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 4, "year" : currentYear]) {(response: AnyObject!, error: NSError!) -> Void in
                                                        if (response != nil && error == nil) {
                                                            
                                                            for(var i = 0; i < response.count; i++) {
                                                                futureMonth[i] = response[i] as Int
                                                                if response[i] as Int == 1
                                                                {
                                                                    NSLog("Response future +3 index + \(i) gives 1")
                                                                }
                                                            }
                                                            
                                                            self.masterContainerPublic[5] = futureMonth
                                                            self.calendarView.contentController.delegate.updateFrames()
                                                            
                                                        } else {
                                                            NSLog("Error attempting to collect parse data to populate future month")
                                                        }
                                                    }
                                                    
                                                } else {
                                                    NSLog("Error attempting to collect parse data to populate future month")
                                                }
                                            }
                                            
                                        } else {
                                            NSLog("Error attempting to collect parse data to populate future month")
                                        }
                                    }
                                    
                                } else {
                                    NSLog("Error attempting to collect parse data to populate future month")
                                }
                            }
                        } else {
                            NSLog("Error attempting to collect parse data to populate past month")
                        }
                    }
                } else {
                    NSLog("Error attempting to collect parse data to populate present month")
                }
                
            }
            
            //Populating SUBSCRIBED events
            //SIX nested parse calls that will populate the current month, past month, and FOUR future months with dots if that day has any events
            PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth , "year" : currentYear, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
                if (response != nil && error == nil) {
                    
                    //Populate present month with 1st call
                    for(var i = 0; i < response.count; i++) {
                        presentMonth[i] = response[i] as Int
                        if response[i] as Int == 1
                        {
                            NSLog("Response Current month call index + \(i) gives 1")
                        }
                    }
                    
                    self.masterContainerSubscribed[1] = presentMonth
                    self.calendarView.contentController.delegate.updateFrames()
                    
                    //2nd Call to populate past month
                    PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth - 1 , "year" : currentYear, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
                        if (response != nil && error == nil) {
                            
                            for(var i = 0; i < response.count; i++) {
                                pastMonth[i] = response[i] as Int
                                if response[i] as Int == 1
                                {
                                    NSLog("Response Past index + \(i) gives 1")
                                }
                            }
                            
                            self.masterContainerSubscribed[0] = pastMonth
                            self.calendarView.contentController.delegate.updateFrames()
                            
                            //3rd Call to populate future month
                            PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 1 , "year" : currentYear, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
                                if (response != nil && error == nil) {
                                    
                                    for(var i = 0; i < response.count; i++) {
                                        futureMonth[i] = response[i] as Int
                                        if response[i] as Int == 1
                                        {
                                            NSLog("Response future index + \(i) gives 1")
                                        }
                                    }
                                    
                                    self.masterContainerSubscribed[2] = futureMonth
                                    self.calendarView.contentController.delegate.updateFrames()
                                    
                                    //4th Call to populate future +1 month
                                    PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 2 , "year" : currentYear, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
                                        if (response != nil && error == nil) {
                                            
                                            for(var i = 0; i < response.count; i++) {
                                                futureMonth[i] = response[i] as Int
                                                if response[i] as Int == 1
                                                {
                                                    NSLog("Response future +1 index + \(i) gives 1")
                                                }
                                            }
                                            
                                            self.masterContainerSubscribed[3] = futureMonth
                                            self.calendarView.contentController.delegate.updateFrames()
                                            
                                            //5th Call to populate future +2 month
                                            PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 3 , "year" : currentYear, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
                                                if (response != nil && error == nil) {
                                                    
                                                    for(var i = 0; i < response.count; i++) {
                                                        futureMonth[i] = response[i] as Int
                                                        if response[i] as Int == 1
                                                        {
                                                            NSLog("Response future +2 index + \(i) gives 1")
                                                        }
                                                    }
                                                    
                                                    self.masterContainerSubscribed[4] = futureMonth
                                                    self.calendarView.contentController.delegate.updateFrames()
                                                    
                                                    //6th Call to populate future +3 month
                                                    PFCloud.callFunctionInBackground("getPublicEventNumbersOnMonth", withParameters: [ "month" : currentMonth + 4, "year" : currentYear, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
                                                        if (response != nil && error == nil) {
                                                            
                                                            for(var i = 0; i < response.count; i++) {
                                                                futureMonth[i] = response[i] as Int
                                                                if response[i] as Int == 1
                                                                {
                                                                    NSLog("Response future +3 index + \(i) gives 1")
                                                                }
                                                            }
                                                            
                                                            self.masterContainerSubscribed[5] = futureMonth
                                                            self.calendarView.contentController.delegate.updateFrames()
                                                            
                                                        } else {
                                                            NSLog("Error attempting to collect parse data to populate future month")
                                                        }
                                                    }
                                                    
                                                } else {
                                                    NSLog("Error attempting to collect parse data to populate future month")
                                                }
                                            }
                                            
                                        } else {
                                            NSLog("Error attempting to collect parse data to populate future month")
                                        }
                                    }
                                    
                                } else {
                                    NSLog("Error attempting to collect parse data to populate future month")
                                }
                            }
                        } else {
                            NSLog("Error attempting to collect parse data to populate past month")
                        }
                    }
                } else {
                    NSLog("Error attempting to collect parse data to populate present month")
                }
                
            }
        }
        self.monthLabel.text = CVDate(date: NSDate()).description()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.calendarView.commitCalendarViewUpdate()
        self.menuView.commitMenuViewUpdate()
    }
    
    // MARK: - IB Actions
    
    @IBAction func todayMonthView() {
        self.calendarView.toggleTodayMonthView()
    }
    
    // MARK: Calendar View Delegate
    
    func shouldShowWeekdaysOut() -> Bool {
        return self.shouldShowDaysOut
    }
    
    func didSelectDayView(dayView: CVCalendarDayView) {
        // TODO:
        var todaysDate:NSDate = NSDate()
        
        if  passedTodaysDateOnce == false
        {
            passedTodaysDateOnce = true
        }
        else
        {
            dateSingleton.sharedInstance.selectedDay = dayView.date
            dateSingleton.sharedInstance.segued = true
            self.tabBarController?.selectedIndex = 1
        }
    }
    
    func dotMarker(colorOnDayView dayView: CVCalendarDayView) -> UIColor {
        /*if dayView.date?.day == 3 {
        return .greenColor()
        }*/
        
        return .redColor()
    }
    
    func dotMarker(shouldShowOnDayView dayView: CVCalendarDayView) -> Bool {
        var markerIsSet : Bool = false
        
        var dotContainer = Array<Array<Int>>()
        
        if subscribedPublicFlag == 0//toggle is ON and calender is showing all public feeds
        {
            dotContainer = masterContainerPublic
        }
        else if subscribedPublicFlag == 1//toggle is off and calender is only showing subscribed feeds
        {
            dotContainer = masterContainerSubscribed
        }
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitMonth | .CalendarUnitYear, fromDate: date)
        var currentYear = components.year
        var currentMonth = components.month//index 1 of master container
        
        var index0Month = currentMonth - 1//keeps track of month at 0th index of container
        
        for monthCounter in 0 ..< dotContainer.count
        {
            for dayCounter in 0 ..< numOfDays{
                if dayView.date?.month == (index0Month + monthCounter) &&
                    dayView.date?.day == (dayCounter) &&
                    dotContainer[monthCounter][dayCounter] == 1
                {
                    markerIsSet = true
                }
            }
        }
        
        return markerIsSet
    }
    
    func dotMarker(shouldMoveOnHighlightingOnDayView dayView: CVCalendarDayView) -> Bool {
        return false
    }
    
    func topMarker(shouldDisplayOnDayView dayView: CVCalendarDayView) -> Bool {
        return true
    }
    
    
    func presentedDateUpdated(date: CVDate) {
        if self.monthLabel.text != date.description() && self.animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .Center
            updatedMonthLabel.text = date.description
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransformMakeTranslation(0, offset)
            updatedMonthLabel.transform = CGAffineTransformMakeScale(1, 0.1)
            
            UIView.animateWithDuration(0.35, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransformMakeTranslation(0, -offset)
                self.monthLabel.transform = CGAffineTransformMakeScale(1, 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransformIdentity
                
                }) { (finished) -> Void in
                    self.animationFinished = true
                    self.monthLabel.frame = updatedMonthLabel.frame
                    self.monthLabel.text = updatedMonthLabel.text
                    self.monthLabel.transform = CGAffineTransformIdentity
                    self.monthLabel.alpha = 1
                    updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
    func toggleMonthViewWithMonthOffset(offset: Int) {
        let calendar = NSCalendar.currentCalendar()
        let calendarManager = CVCalendarManager.sharedManager
        let components = calendarManager.componentsForDate(NSDate()) // from today
        
        components.month += offset
        
        let resultDate = calendar.dateFromComponents(components)!
        
        self.calendarView.toggleMonthViewWithDate(resultDate)
    }
    
    func setMasterContainerPublic(){
        
    }
    
    func get2dArrayOfZeros() -> Array<Array<Int>> {
        
        var array2d: [[Int]] =
        [
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
        ]
        return array2d
    }
}