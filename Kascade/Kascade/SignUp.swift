//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class SignUp: UIViewController {
    
    @IBOutlet weak var usernameText : UITextField!
    @IBOutlet weak var passwordText : UITextField!
    @IBOutlet weak var emailText : UITextField!
    
    @IBAction func selectUsername() {
        usernameText.becomeFirstResponder()
    }
    @IBAction func selectPassword() {
        passwordText.becomeFirstResponder()
    }
    @IBAction func selectEmail() {
        emailText.becomeFirstResponder()
    }
    @IBAction func letGoEmail() {
        emailText.resignFirstResponder()
    }
    @IBAction func letGoUsername() {
        usernameText.resignFirstResponder()
    }
    @IBAction func letGoPassword() {
        passwordText.resignFirstResponder()
    }

    @IBAction func signUp() {
        if Reachability.isConnectedToNetwork() {
            var newUser = PFUser()
            newUser.username = usernameText.text
            newUser.password = passwordText.text
            newUser.email = emailText.text
            newUser.signUpInBackgroundWithBlock {
                (succeeded: Bool , error:NSError!) -> Void in
                if succeeded && error == nil {
                    let alert = UIAlertController(title:"", message:"Please make sure to verify your email.", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Continue", style: .Default, handler: {(alertAction) -> Void in self.moveOn()}))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    let alert = UIAlertController(title:"", message:"The username or email you used is already taken.", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
            }
        }
        else {
            let alert = UIAlertController(title:"", message:"We can't find the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
    }

    func moveOn() {
        performSegueWithIdentifier("signup", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
