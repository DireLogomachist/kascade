//
//  DayView.swift
//  Kascade
//Copyright 2015 Miller and Grove City College. All rights reserved
// Mean Machine
//Miller, Andy
//4/26/15

import UIKit

class DayView: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var dayFeedView: UITableView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    var loading = false
    var scrolling = false
    var publicSwitch = true
    var currentDateIndex : NSDate!
    
    var eventToPass : event!
    
    
    @IBAction func nextPress(sender: AnyObject) {
        if(!loading) {
            let nextDate = NSCalendar.currentCalendar().dateByAddingUnit(.CalendarUnitDay, value: 1, toDate: currentDateIndex, options: NSCalendarOptions(0))
            currentDateIndex = nextDate
            
            var format = NSDateFormatter()
            format.dateStyle = NSDateFormatterStyle.LongStyle
            dayLabel.text = format.stringFromDate(currentDateIndex)
            
            //reload table on switch state
            loadPublicParseData()
            loadSubscribedParseData()
        }
    }
    
    @IBAction func prevPress(sender: AnyObject) {
        if(!loading) {
            let prevDate = NSCalendar.currentCalendar().dateByAddingUnit(.CalendarUnitDay, value: -1, toDate: currentDateIndex, options: NSCalendarOptions(0))
            currentDateIndex = prevDate
            
            var format = NSDateFormatter()
            format.dateStyle = NSDateFormatterStyle.LongStyle
            dayLabel.text = format.stringFromDate(currentDateIndex)
            
            //reload table on switch state
            loadPublicParseData()
            loadSubscribedParseData()
        }
    }

    @IBOutlet weak var publicFeeds: UISwitch!
    
    @IBAction func publicSwitchChanged(sender: AnyObject) {
        //Reload table with correct eventList
        publicSwitch = publicFeeds.on
        dayFeedView.reloadData()
    }
    
    //Testing the reloadData method instead of the insertRowsAtPath method
    struct event {
        var name : String = ""
        var group : String = ""
        var location : String = ""
        var time : String = ""
        var objectId : String = ""
    }
    
    var eventListPublic = Array<Array<event>>()
    var eventListSubscribed = Array<Array<event>>()
    var hours = ["12:00 AM", "1:00 AM", "2:00 AM", "3:00 AM", "4:00 AM", "5:00 AM", "6:00 AM", "7:00 AM", "8:00 AM", "9:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "1:00 PM", "2:00 PM", "3:00 PM", "4:00 PM", "5:00 PM", "6:00 PM", "7:00 PM", "8:00 PM", "9:00 PM", "10:00 PM", "11:00 PM",]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        //self.navigationController?.navigationBar.topItem?.title = "Day"
        
        dayFeedView.delegate = self
        dayFeedView.dataSource = self
        publicSwitch = publicFeeds.on
        
        for(var i = 0; i < 24; i++) {
            eventListPublic.append(Array<event>())
            eventListSubscribed.append(Array<event>())
        }
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
        var asdf = self.tabBarController?.selectedIndex.description
        //NSLog("SELCTED: " + asdf!)
        
        
        if(dateSingleton.sharedInstance.segued != nil && dateSingleton.sharedInstance.segued == true) {
        //extact NSDate from CVDate
        var newDateComps = NSDateComponents()
        newDateComps.month = dateSingleton.sharedInstance.selectedDay!.month!
        newDateComps.year = dateSingleton.sharedInstance.selectedDay!.year!
        newDateComps.day = dateSingleton.sharedInstance.selectedDay!.day!
        
        //Update currentDayIndex
        let newDate = NSCalendar.currentCalendar().dateFromComponents(newDateComps)
        currentDateIndex = newDate!
        
        } else {
        //currentDayIndex should be left alone
        }
        
        if Reachability.isConnectedToNetwork() {
            //var formatter = NSDateFormatter()
            //formatter.dateFormat = "MM/dd/yy"
            //var d : NSDate = formatter.dateFromString("04/14/15")!
            
            if(currentDateIndex == nil) {
                var cal  = NSCalendar(calendarIdentifier: NSGregorianCalendar)
                let d = cal?.startOfDayForDate(NSDate())
                currentDateIndex  = d
            }
            
            var format = NSDateFormatter()
            format.dateStyle = NSDateFormatterStyle.LongStyle
            dayLabel.text = format.stringFromDate(currentDateIndex)
            
            
            loadPublicParseData()
            loadSubscribedParseData()
        }  else {
            let alert = UIAlertController(title:"", message:"Internet Connection Error", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : EventCell = tableView.dequeueReusableCellWithIdentifier("feedCell", forIndexPath: indexPath) as EventCell
        
        if(publicSwitch) {
            cell.name.text = eventListPublic[indexPath.section][indexPath.row].name
            cell.time.text = eventListPublic[indexPath.section][indexPath.row].time
            cell.location.text = eventListPublic[indexPath.section][indexPath.row].location
            cell.group.text = eventListPublic[indexPath.section][indexPath.row].group
        } else {
            cell.name.text = eventListSubscribed[indexPath.section][indexPath.row].name
            cell.time.text = eventListSubscribed[indexPath.section][indexPath.row].time
            cell.location.text = eventListSubscribed[indexPath.section][indexPath.row].location
            cell.group.text = eventListSubscribed[indexPath.section][indexPath.row].group
        }
        
        cell.sizeToFit()
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!publicSwitch) {
            return eventListSubscribed[section].count
        }
        return eventListPublic[section].count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 24
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.hours[section]
    }
    
    func setLoadingState(loading:Bool) {
        self.loading = loading
        //feedView.tableFooterView?.hidden = !loading
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(publicSwitch) {
            eventToPass = event()
            eventToPass.name = eventListPublic[indexPath.section][indexPath.row].name
            eventToPass.time = eventListPublic[indexPath.section][indexPath.row].time
            eventToPass.location = eventListPublic[indexPath.section][indexPath.row].location
            eventToPass.group = eventListPublic[indexPath.section][indexPath.row].group
            eventToPass.objectId = eventListPublic[indexPath.section][indexPath.row].objectId
        } else {
            eventToPass = event()
            eventToPass.name = eventListSubscribed[indexPath.section][indexPath.row].name
            eventToPass.time = eventListSubscribed[indexPath.section][indexPath.row].time
            eventToPass.location = eventListSubscribed[indexPath.section][indexPath.row].location
            eventToPass.group = eventListSubscribed[indexPath.section][indexPath.row].group
            eventToPass.objectId = eventListSubscribed[indexPath.section][indexPath.row].objectId
        }
    
        performSegueWithIdentifier("dayToDetail", sender: self)
        dayFeedView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "dayToDetail") {
            var view = segue.destinationViewController as EventDetails
            
            view.namePassed = eventToPass.name
            view.datePassed = eventToPass.time
            view.locationPassed = eventToPass.location
            view.groupPassed = eventToPass.group
            view.idPassed = eventToPass.objectId
        }
    }
    
    
    func loadPublicParseData() {
        setLoadingState(true)
        
        for var i = 0; i < 24; i++ {
            self.eventListPublic[i].removeAll(keepCapacity: false)
        }
        
        var day : NSDate!
        //Call getPublicEventsBatch
        PFCloud.callFunctionInBackground("getPublicEventsOnDay", withParameters: ["date" : currentDateIndex ]) {(response: AnyObject!, error: NSError!) -> Void in
            
            if error == nil {
                //Empty response
                if response.description == "Empty" {
                    //NSLog(response.description)
                    return
                }
            
                //Loop through retrieved data and add cells to tableView
                for var i = 0; i < response.count; i++ {
                    var ev : event! = event()
                    ev.name = (String)(response[i]["name"] as String)
                    ev.group = (String)(response[i]["groupName"] as String)
                    ev.location = (String)(response[i]["location"] as String)
                    ev.objectId = (String)(response[i]!.objectId as String)
                    
                    var formatter = NSDateFormatter()
                    formatter.dateFormat = "h:mm a - MM/dd/yy"
                    day = response[i]["date"] as NSDate
                    let date : String = formatter.stringFromDate(day)
                    ev.time = date
                    
                    let calendar = NSCalendar.currentCalendar()
                    let comp = calendar.components((.HourCalendarUnit | .MinuteCalendarUnit), fromDate: day)
                    
                    self.eventListPublic[comp.hour].append(ev)
                }
                
                //At the end of additions reload data, set loading state off, and move date indices
                self.dayFeedView.reloadData()
                self.setLoadingState(false)
            } else {
                //Error auto-logs
            }
        }
    }
    
    func loadSubscribedParseData() {
        setLoadingState(true)
        
        for var i = 0; i < 24; i++ {
            eventListSubscribed[i].removeAll(keepCapacity: false)
        }
        
        
        var day : NSDate!
        //Call getPublicEventsBatch
        PFCloud.callFunctionInBackground("getSubscribedEventsOnDay", withParameters: ["date" : currentDateIndex, "userID" : PFUser.currentUser().objectId]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                
                //Empty response
                if response.description == "Empty" {
                    //NSLog(response.description)
                    return
                }
            
                
                //Loop through retrieved data and add cells to tableView
                for var i = 0; i < response.count; i++ {
                    var ev : event! = event()
                    ev.name = (String)(response[i]["name"] as String)
                    ev.group = (String)(response[i]["groupName"] as String)
                    ev.location = (String)(response[i]["location"] as String)
                    ev.objectId = (String)(response[i]!.objectId) as String
                    
                    var formatter = NSDateFormatter()
                    formatter.dateFormat = "h:mm a - MM/dd/yy"
                    day = response[i]["date"] as NSDate
                    let date : String = formatter.stringFromDate(day)
                    ev.time = date
                    
                    let calendar = NSCalendar.currentCalendar()
                    let comp = calendar.components((.HourCalendarUnit | .MinuteCalendarUnit), fromDate: day)
                    self.eventListSubscribed[comp.hour].append(ev)
                }
                
                //At the end of additions reload data, set loading state off, and move date indices
                self.dayFeedView.reloadData()
                self.setLoadingState(false)
            } else {
                //Error auto-logs
            }
        }
    }

}
