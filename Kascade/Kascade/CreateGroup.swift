//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class CreateGroup: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    //Ethan Tingley
    var groupNameToPass : String = ""
    var groupIDToPass : String = ""
    
    var adminEmail : String = ""
    var adminID : String = ""
    var adminGroup : String = ""
    
    @IBOutlet weak var groupNameText : UITextField!
    @IBOutlet weak var groupDescriptionText : UITextField!
    @IBOutlet weak var uploadPhotoButton : UIButton!
    var groupPhoto : UIImage!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var finishButton : UIButton!
    
    
    @IBAction func selectGroupName() {
        groupNameText.becomeFirstResponder()
    }
    @IBAction func selectDescription() {
        groupDescriptionText.becomeFirstResponder()
    }
    @IBAction func letGoGroupName() {
        groupNameText.resignFirstResponder()
    }
    @IBAction func letGoDescription() {
        groupDescriptionText.resignFirstResponder()
    }
    
    @IBAction func addGroup() {
        
        finishButton.enabled = false
        
        if groupNameText.text.isEmpty || groupDescriptionText.text.isEmpty || imageView.image == nil{
            let alert = UIAlertController(title:"", message: "Please complete all fields.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        let imageData = UIImagePNGRepresentation(groupPhoto)
        let imageFile: PFFile = PFFile(data: imageData)
        
        let newGroup = PFObject(className: "Group")
        let newAdmin = PFObject(className: "Admins")
        let newSubscribe = PFObject(className: "Subscribed")
        
        newGroup["name"] = groupNameText.text
        newGroup["description"] = groupDescriptionText.text
        newGroup["headerImage"] = imageFile
        newGroup["subscribers"] = 0
        
        PFCloud.callFunctionInBackground("checkUniqueGroup", withParameters: ["groupName" : groupNameText.text]){(response: AnyObject!, error: NSError!) -> Void in
            if response != nil && error == nil {
                newGroup.incrementKey("subscribers")
                newGroup.saveInBackgroundWithBlock {
                    (success: Bool, error: NSError!) -> Void in
                    if error == nil {
                        newAdmin["email"] = PFUser.currentUser().email
                        newAdmin["userID"] = PFUser.currentUser()
                        newAdmin["group"] = newGroup
                        
                        newAdmin.saveInBackgroundWithBlock {
                            (success: Bool, error: NSError!) -> Void in
                            if success && error == nil {
                                self.groupNameToPass = newGroup["name"] as String
                                self.groupIDToPass = newGroup.objectId
                                
                                newSubscribe["group"] = newGroup
                                newSubscribe["privateAccess"] = false
                                newSubscribe["user"] = PFUser.currentUser()
                                
                                newSubscribe.saveInBackgroundWithBlock {
                                    (success: Bool, error: NSError!) -> Void in
                                    if success && error == nil {
                                        self.finishButton.enabled = true
                                        self.performSegueWithIdentifier("createToAdmin", sender: self)
                                    }
                                    else {
                                        let alert = UIAlertController(title:"", message: "Something went wrong saving a subscriber.", preferredStyle: .Alert)
                                        alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
                                        self.presentViewController(alert, animated: true, completion: nil)
                                        return
                                    }
                                }
                            }
                            else {
                                let alert = UIAlertController(title:"", message: "Something went wrong saving an admin.", preferredStyle: .Alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
                                self.presentViewController(alert, animated: true, completion: nil)
                                return
                            }
                        }
                    }
                    else {
                        let alert = UIAlertController(title:"", message: "Something went wrong saving a group.", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                }
            }
            else {
                let alert = UIAlertController(title:"", message: "\(self.groupNameText.text) is already taken.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Enter Another", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }

    @IBAction func pickGroupPhoto(AnyObject)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            var imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            //imag.mediaTypes = [kUTTypeImage];
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        let selectedImage : UIImage = image
        //var tempImage:UIImage = editingInfo[UIImagePickerControllerOriginalImage] as UIImage
        self.imageView.image = selectedImage
        self.groupPhoto = selectedImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "createToAdmin") {
            let temp = segue.destinationViewController as AdminPage
            temp.passedGroupName = groupNameToPass
            temp.passedGroupID = groupIDToPass
        }
    }
    
    //end: Ethan tingley

    override func viewDidLoad() {
        super.viewDidLoad()

        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        //forces white background color
        self.view.backgroundColor = UIColor.whiteColor()
        
        self.navigationController?.navigationBar.topItem?.title = "Create Group"
        
        //adminEmail = PFUser.currentUser().email
        //adminID = PFUser.currentUser().objectId
        
        if Reachability.isConnectedToNetwork() {
        }
        else {
            let alert = UIAlertController(title:"", message:"We can't find the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
        
        imageView.image = image
        
    }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
