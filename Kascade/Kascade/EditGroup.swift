//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class EditGroup: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var passedGroupName : String = ""
    var passedGroupID : String = ""
    
    var groupNameToPass : String = ""
    var groupIDToPass : String = ""
    
    var currentGroup = PFObject(className: "Group")!
    
    @IBOutlet weak var groupNameText : UITextField!
    @IBOutlet weak var groupDescriptionText : UITextField!
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var finish : UIButton!
    var groupPhoto : UIImage!
    
    @IBAction func selectGroupName() {
        groupNameText.becomeFirstResponder()
    }
    @IBAction func selectDescription() {
        groupDescriptionText.becomeFirstResponder()
    }
    @IBAction func letGoGroupName() {
        groupNameText.resignFirstResponder()
    }
    @IBAction func letGoDescription() {
        groupDescriptionText.resignFirstResponder()
    }
    
    @IBAction func tapFinish() {
        
        finish.enabled = false
        
        if groupNameText.text.isEmpty || groupDescriptionText.text.isEmpty || imageView.image == nil {
            let alert = UIAlertController(title:"", message: "Please fill in all fields.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        let imageData = UIImagePNGRepresentation(self.imageView.image)
        let imageFile : PFFile = PFFile(data: imageData)
        
        currentGroup["name"] = groupNameText.text
        currentGroup["description"] = groupDescriptionText.text
        currentGroup["headerImage"] = imageFile
        groupNameToPass = currentGroup["name"] as String
        groupIDToPass = currentGroup.objectId
        
        currentGroup.saveInBackgroundWithBlock {
            (success: Bool, error: NSError!) -> Void in
            if success && error == nil {
                self.finish.enabled = true
                let alert = UIAlertController(title:"", message: "Group information has been saved.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Continue", style: .Default, handler: {(alertAction) -> Void in self.moveOn()}))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else {
                let alert = UIAlertController(title:"", message: "Could not save group data", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    func moveOn() {
        self.performSegueWithIdentifier("toAdmin", sender: self)
    }

    @IBAction func pickGroupPhoto(AnyObject)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            var imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            //imag.mediaTypes = [kUTTypeImage];
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        let selectedImage : UIImage = image
        //var tempImage:UIImage = editingInfo[UIImagePickerControllerOriginalImage] as UIImage
        self.imageView.image = selectedImage
        self.groupPhoto = selectedImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func loadGroupToEdit() {
        var query = PFQuery(className: "Group")
        query.getObjectInBackgroundWithId("\(passedGroupID)") {
            (Group: PFObject!, error: NSError!) -> Void in
            if error == nil && Group != nil {
                self.currentGroup = Group
                self.groupNameText.text = self.currentGroup["name"] as String
                self.groupDescriptionText.text = self.currentGroup["description"] as String
                
                var tempImageFile : PFFile!
                var tempUIImage : UIImage!
                tempImageFile = self.currentGroup["headerImage"] as PFFile
                tempImageFile.getDataInBackgroundWithBlock {
                    (imageData: NSData!, error: NSError!) -> Void in
                    if error == nil {
                        tempUIImage = UIImage(data: imageData)
                        self.imageView.image = tempUIImage
                    }
                }
            }
            else {
                let alert = UIAlertController(title:"", message: "Could not retrieve group data", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toAdmin" {
            let temp = segue.destinationViewController as AdminPage
            temp.passedGroupName = groupNameToPass
            temp.passedGroupID = groupIDToPass
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()

        groupNameToPass = passedGroupName
        groupIDToPass = passedGroupID
        
        if Reachability.isConnectedToNetwork() {
            loadGroupToEdit()
        }
        else {
            let alert = UIAlertController(title:"", message:"We can't find the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }

        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
