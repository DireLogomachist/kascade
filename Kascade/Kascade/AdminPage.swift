//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/21/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class AdminPage: UIViewController {
    
    var passedGroupName : String = ""
    var passedGroupID : String = ""
    
    var groupNameToPass : String = ""
    var groupIDToPass : String = ""
    
    @IBOutlet weak var groupLabel : UILabel!
    @IBOutlet weak var imageView : UIImageView!
    var currentGroup = PFObject(className: "Group")!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()

        groupLabel.text = passedGroupName
        
        groupNameToPass = passedGroupName
        groupIDToPass = passedGroupID
        
        loadImage()
        
        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        // Do any additional setup after loading the view.
    }
    
    func loadImage() {
        var query = PFQuery(className: "Group")
        query.whereKey("name", equalTo: passedGroupName)
        query.getFirstObjectInBackgroundWithBlock {
            (Group: PFObject!, error: NSError!) -> Void in
            if Group != nil && error == nil {
                self.currentGroup = Group
                
                var tempImageFile : PFFile!
                var tempUIImage : UIImage!
                tempImageFile = self.currentGroup["headerImage"] as PFFile
                tempImageFile.getDataInBackgroundWithBlock {
                    (imageData: NSData!, error: NSError!) -> Void in
                    if error == nil {
                        tempUIImage = UIImage(data: imageData)
                        self.imageView.image = tempUIImage
                    }
                }
            }
        }

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "adminToEvent") {
            let temp = segue.destinationViewController as CreateEvent
            temp.passedGroupID = groupIDToPass
            temp.passedGroupName = groupNameToPass
        }
        else if(segue.identifier == "adminToEdit") {
            let temp = segue.destinationViewController as EditGroup
            temp.passedGroupID = groupIDToPass
            temp.passedGroupName = groupNameToPass
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toAdmin(segue: UIStoryboardSegue) {
        let source = segue.sourceViewController as EditGroup
        passedGroupName = source.groupNameToPass
        passedGroupID = source.groupIDToPass
        self.loadImage()
    }
    
    @IBAction func toAdmin2(segue: UIStoryboardSegue) {
        let source = segue.sourceViewController as RecurrenceOptions
        passedGroupName = source.groupNameToPass
        passedGroupID = source.groupIDToPass
        self.loadImage()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
