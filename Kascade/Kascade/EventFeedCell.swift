//  Kascade
//Copyright 2015 Miller and Grove City College. All rights reserved
// Mean Machine
//Miller, Andy
//4/12/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class EventFeedCell: UITableViewCell {

    var eventData : PFObject!
    
    var name : UILabel!
    var location : UILabel!
    var time : UILabel!
    var group : UILabel!
    
    var borders : UIView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.lightTextColor()
        self.contentView.layer.borderColor = UIColor.blueColor().CGColor
        self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        /*//BORDER EXPERIMENTS
        //self.contentView.layer.borderWidth = 1.0
        borders = UIView()
        //borders.frame = self.frame
        borders.bounds = self.bounds
        borders.layer.borderColor = UIColor.blueColor().CGColor
        borders.layer.borderWidth = 2.0
        */
        
        let xOffset : CGFloat = 10.0
        let yOffset : CGFloat = 24.0
        let initY : CGFloat = 32.0
        let initX : CGFloat = 12.0

        
        name = UILabel(frame: CGRectMake(self.frame.origin.x + initX, self.frame.origin.y - initY, 100, 100))
        location = UILabel(frame: CGRectMake(self.frame.origin.x + initX, self.frame.origin.y - initY + yOffset, 100, 100))
        time = UILabel(frame: CGRectMake(self.frame.origin.x + initX + 100, self.frame.origin.y - initY + yOffset, 100, 100))
        group = UILabel(frame: CGRectMake(self.frame.origin.x + initX, self.frame.origin.y - initY + yOffset*2, 100, 100))
        
        name.text = "Name"
        location.text = "Location"
        time.text = "Time"
        group.text = "Group"
        
        //self.addSubview(borders)
        self.addSubview(name)
        self.addSubview(location)
        self.addSubview(time)
        self.addSubview(group)
        
    }


    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
