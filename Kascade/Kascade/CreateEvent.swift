//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/15/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class CreateEvent: UIViewController, RecurDelegate {
    
    //Ethan tingley
    var passedGroupName : String = ""
    var passedGroupID : String = ""
    
    var groupNameToPass : String!
    var groupIDToPass : String!
    
    @IBOutlet weak var eventNameText : UITextField!
    @IBOutlet weak var eventDescriptionText : UITextField!
    @IBOutlet weak var eventLocationText : UITextField!
    @IBOutlet weak var eventDate : UIDatePicker!
    @IBOutlet weak var eventPublicSwitch : UISwitch!
    
    @IBAction func selectName() {
        eventNameText.becomeFirstResponder()
    }
    @IBAction func selectLocation() {
        eventLocationText.becomeFirstResponder()
    }
    @IBAction func selectDescription() {
        eventDescriptionText.becomeFirstResponder()
    }
    @IBAction func letGoName() {
        eventNameText.resignFirstResponder()
    }
    @IBAction func letGoLocation() {
        eventLocationText.resignFirstResponder()
    }
    @IBAction func letGoDescription() {
        eventDescriptionText.resignFirstResponder()
    }
    @IBAction func moveToRecur() {
        performSegueWithIdentifier("toRecur", sender: self)
    }
    
    //end: Ethan Tingley
    
    func toCreateEvent(segue: UIStoryboardSegue) {}

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        
        groupNameToPass = passedGroupName
        groupIDToPass = passedGroupID

        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if eventNameText.text.isEmpty || eventLocationText.text.isEmpty || eventDescriptionText.text.isEmpty {
            let alert = UIAlertController(title:"", message: "Please fill in all fields.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        if(segue.identifier == "toRecur")
        {
            let recur = segue.destinationViewController as RecurrenceOptions
            recur.passedGroupID = groupIDToPass
            recur.passedGroupName = groupNameToPass
            
            recur.delegate = self
            recur.eventName = eventNameText.text
            recur.eventLocation = eventLocationText.text
            recur.eventDescription = eventDescriptionText.text
            recur.eventDate = eventDate.date
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
