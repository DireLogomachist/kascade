//AppDelegate.swift
//Andy Miller
//Last edited: 4/10/15
//Significant changes:
//  added Parse Tests to run on start
//  added even more Parse tests to run on start

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //Set up for the Kascade_2.0 Parse project
        Parse.setApplicationId("oOokbBxFdrLm72hZlCrjJeGJs89yNyV9ZjB6IQYv", clientKey: "4pry1fJluOEpv9VWwg80fDtPhebZSKsro6DAhpiS")
        
        //Navigation Bar Text Color to white
        var navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        
        //Parse backend testing
        testsAllParseFunctions()
        
        return true
    }

   
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //DEPRECATED PARSE TESTS - DO NOT RUN
    func parseObjectTests(objectID: String) {
        
        //Test 1
        PFCloud.callFunctionInBackground("getGroup", withParameters: ["groupID" : objectID]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response["name"] as String == "Delta Rho Sigma") {
                    var str : String = response["name"] as String
                    NSLog("Test 1 - Correct group name retrieved: \(str)");
                } else {
                    NSLog("Test 1 - Failed to retrieve group name")
                }
            }
        }
        /*
        //Test 2 - DO NOT RUN - breaks on computers other than Andy's
        var newGroup = PFObject(className: "Group")
        var img : PFFile = PFFile(name: "tf2.jpg", contentsAtPath: "/Users/DireLogomachist/Downloads/tf2.jpg")
        newGroup["name"] = "Students for Liberty"
        newGroup["description"] = "Faith and Freedom and Stuff"
        newGroup["headerImage"] = img
        newGroup["subscribers"] = 0
        
        newGroup.saveInBackgroundWithBlock {
            (success: Bool, error: NSError!) -> Void in
            if success {
                NSLog("Test 2 - New Group successfully added")
            } else {
                NSLog("Test 2 - Failed to add New Group")
            }
            
        }*/
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Test 3
        PFCloud.callFunctionInBackground("getGroupByName", withParameters: ["groupName" : "Students for Liberty"]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response["name"] as String? == "Students for Liberty") {
                    var str : String = response["description"] as String
                    NSLog("Test 3 - Stored group description retrieved: \(str)");
                } else {
                    NSLog("Test 3 - Failed to retrieve stored group description")
                }
            }
        }
    }
    
    //============================================================================================================================

    //New and approved parse tests
    func testsAllParseFunctions() {
        
        var objectID : String = "CJUEUDep2a"
        var eventID : String = "FZDPWCX4x5"
        var userID : String = "2vamLu4NOG"
        var uniqueGroup : String = "Night Vale Chess Club"
        var uniqueUsername : String = "CarcinoGeneticist"
        
        var formatter = NSDateFormatter()
        formatter.dateFormat = "MM/dd/yy"
        var date : NSDate = formatter.dateFromString("04/13/15")!
        //var prevDate : NSDate = formatter.dateFromString("02/15/15")!
        //var nextDate : NSDate = formatter.dateFromString("02/17/15")!
        
        //Testing getGroup()
        PFCloud.callFunctionInBackground("getGroup", withParameters: ["groupID" : objectID]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response["name"] as String == "Delta Rho Sigma") {
                    var str : String = response["name"] as String
                    NSLog("Parse Test 1 passed - getGroup function: \(str)");
                } else {
                    NSLog("Parse Test 1 failed - getGroup function")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //TestingGetGroupByName()
        PFCloud.callFunctionInBackground("getGroupByName", withParameters: ["groupName" : "Delta Rho Sigma"]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response.objectId as String == objectID) {
                    var str : String = response.objectId as String
                    NSLog("Parse Test 2 passed - getGroupByName function: \(str)");
                } else {
                    NSLog("Parse Test 2 failed - getGroupByName function")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing getAllGroupNames() - Only will work for current groups - any more added will break test
        PFCloud.callFunctionInBackground("getAllGroupNames", withParameters: [ : ]){(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if((response[0]["name"] as String == "Delta Rho Sigma" && response[1]["name"] as String == "Students for Liberty")||(response[1]["name"] as String == "Delta Rho Sigma" && response[0]["name"] as String == "Students for Liberty")) {
                    let str1 : String = response[0]["name"] as String
                    let str2 : String = response[1]["name"] as String
                    NSLog("Parse Test 3 passed - getAllGroupNames function: \(str1), \(str2)")
                } else {
                    NSLog("Parse Test 3 failed - getAllGroupNames function")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing getEventsByGroup()
        PFCloud.callFunctionInBackground("getEventsByGroup", withParameters: ["groupID" : objectID]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response.count as Int == 2  && ((response[0]["name"] as String == "Meatpocalypse" && response[1]["name"] as String == "Buffalo Open House")||(response[1]["name"] as String == "Meatpocalypse" && response[0]["name"] as String == "Buffalo Open House"))){
                    let str1 : String = response[0]["name"] as String
                    let str2 : String = response[1]["name"] as String
                    NSLog("Parse Test 4 passed - getEventsByGroup function: \(str1), \(str2)")
                } else {
                    NSLog("Parse Test 4 failed - getEventsByGroup function")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing getNumberGroupEvents()
        PFCloud.callFunctionInBackground("getNumberGroupEvents", withParameters: ["groupID" : objectID]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response as Int == 2) {
                    let str : String = "\(response as Int)"
                    NSLog("Parse Test 5 passed - getNumberGroupEvents function: \(str) events")
                } else {
                    let str : String = "\(response as Int)"
                    NSLog("Parse Test 5 failed - getNumberGroupEvents function:  \(str) event(s)")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing getNumberSubscribers()
        PFCloud.callFunctionInBackground("getNumberSubscribers", withParameters: ["groupID" : objectID]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response as Int == 579) {
                    var str : String = "\(response as Int)"
                    NSLog("Parse Test 6 passed - getNumberSubscribers function: Number of Subscribers - \(str)");
                } else {
                    NSLog("Parse Test 6 failed - getNumberSubscribers function")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing getEvent()
        PFCloud.callFunctionInBackground("getEvent", withParameters: ["eventID" : eventID]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response["name"] as String == "Buffalo Open House") {
                    var str : String = response["name"] as String
                    NSLog("Parse Test 7 passed - getEvent function: \(str)");
                } else {
                    NSLog("Parse Test 7 failed - getEvent function")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing getEventsOnDay()
        PFCloud.callFunctionInBackground("getEventsOnDay", withParameters: ["date" : date]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if(response.count == 1 && response[0]["name"] as String == "Buffalo Open House") {
                    var str : String = response[0]["name"] as String
                    NSLog("Parse Test 8 passed - getEventsOnDay function: \(str)");
                } else {
                    NSLog("Parse Test 8 failed - getEventsOnDay function")
                }
            } else {
                //Error auto-logs
                NSLog("Something up? - Test 8")
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing getSubscribedEvents()
        PFCloud.callFunctionInBackground("getSubscribedEvents", withParameters: ["userID" : userID]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                if((response[0]["name"] as String == "Meatpocalypse" && response[1]["name"] as String == "Buffalo Open House")||(response[1]["name"] as String == "Meatpocalypse" && response[0]["name"] as String == "Buffalo Open House")) {
                    let str1 : String = response[0]["name"] as String
                    let str2 : String = response[1]["name"] as String
                    NSLog("Parse Test 9 passed - getSubscribedEvents function: \(str1), \(str2)")
                } else {
                    NSLog("Parse Test 9 failed - getSubscribedEvents function")
                }
            } else {
                //Error auto-logs
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing checkUniqueGroup()
        PFCloud.callFunctionInBackground("checkUniqueGroup", withParameters: ["groupName" : uniqueGroup]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                NSLog("Parse Test 10 passed - checkUniqueGroup function: groupName '\(uniqueGroup)' is unique")
            } else {
                //Error auto-logs
                NSLog("Parse Test 10 failed - checkUniqueGroup function: groupName '\(uniqueGroup)' is not unique")
            }
        }
        
        NSThread.sleepForTimeInterval(1.0)
        
        //Testing checkUniqueUsername()
        PFCloud.callFunctionInBackground("checkUniqueUsername", withParameters: ["userName" : uniqueUsername]) {(response: AnyObject!, error: NSError!) -> Void in
            if error == nil {
                NSLog("Parse Test 11 passed - checkUniqueUsername function: userName '\(uniqueUsername)' is unique")
            } else {
                //Error auto-logs
                NSLog("Parse Test 11 failed - checkUniqueUsername function: userName '\(uniqueUsername)' is not unique")
            }
        }
        
        //MORE TESTS...
        
        
        
        
    }
}








