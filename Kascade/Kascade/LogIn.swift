//
//  LogIn.swift
//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/10/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

class LogIn: UIViewController {
    
    @IBOutlet weak var usernameText : UITextField!
    @IBOutlet weak var passwordText : UITextField!
    
    struct user {
        var username : String = ""
        var password : String = ""
    }
    var userCheck : [user] = [user]()
    
    
    @IBAction func selectUsername() {
        usernameText.becomeFirstResponder()
    }
    @IBAction func selectPassword() {
        passwordText.becomeFirstResponder()
    }
    @IBAction func letGoUsername() {
        usernameText.resignFirstResponder()
    }
    @IBAction func letGoPassword() {
        passwordText.resignFirstResponder()
    }
    @IBAction func tapLogIn() {
        login()
    }
    
    func login() {
        if Reachability.isConnectedToNetwork() {
            PFUser.logInWithUsernameInBackground(usernameText.text, password: passwordText.text) {(user: PFUser!, error: NSError!) -> Void in
                if user != nil && error == nil {
                    self.performSegueWithIdentifier("login", sender: self)
                }
                else {
                    let alert = UIAlertController(title:"", message:"Username and/or password incorrect.", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
        else {
            let alert = UIAlertController(title:"", message:"We can't find the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logOut(segue: UIStoryboardSegue) {}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
