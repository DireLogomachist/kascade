//  Kascade
//Copyright 2015 Tingley and Grove City College. All rights reserved
// Mean Machine
//Tingley, Ethan
//4/26/15
//last data modified
// list of significant changes
//
//date when unit tested and passed and TesterLastName
//date when integration tested and passed and TesterLastName

import UIKit

protocol RecurDelegate {
    
}

class RecurrenceOptions: UIViewController {
    
    var passedGroupName : String = ""
    var passedGroupID : String = ""
    
    var groupNameToPass : String = ""
    var groupIDToPass : String = ""
    
    var currentGroup = PFObject(className: "Group")!
    
    @IBOutlet weak var endByDate : UIDatePicker!
    @IBOutlet weak var sundaySwitch : UISwitch!
    @IBOutlet weak var mondaySwitch : UISwitch!
    @IBOutlet weak var tuesdaySwitch : UISwitch!
    @IBOutlet weak var wednesdaySwitch : UISwitch!
    @IBOutlet weak var thursdaySwitch : UISwitch!
    @IBOutlet weak var fridaySwitch : UISwitch!
    @IBOutlet weak var saturdaySwitch : UISwitch!
    @IBOutlet weak var finish : UIButton!
    
    var eventName : String!
    var eventDescription : String!
    var eventLocation : String!
    var eventDate : NSDate!
    var eventRecurring = [Int](count: 7, repeatedValue: 0)
    
    var delegate : RecurDelegate!
    
    @IBAction func addEvent() {
        finish.enabled = false
        
        let newEvent = PFObject(className: "Event")
        newEvent["name"] = eventName
        newEvent["groupName"] = currentGroup["name"]
        newEvent["description"] = eventDescription
        newEvent["location"] = eventLocation
        newEvent["date"] = eventDate
        newEvent["private"] = false
        newEvent["group"] = currentGroup
        
        if(sundaySwitch.on || mondaySwitch.on || tuesdaySwitch.on || wednesdaySwitch.on || thursdaySwitch.on
           || fridaySwitch.on || saturdaySwitch.on)
        {
            newEvent["recurring"] = true
        }
        else {
            newEvent["recurring"] = false
        }
        
        if sundaySwitch.on {
            eventRecurring[0] = 1
        }
        if mondaySwitch.on {
            eventRecurring[1] = 1
        }
        if tuesdaySwitch.on {
            eventRecurring[2] = 1
        }
        if wednesdaySwitch.on {
            eventRecurring[3] = 1
        }
        if thursdaySwitch.on {
            eventRecurring[4] = 1
        }
        if fridaySwitch.on {
            eventRecurring[5] = 1
        }
        if saturdaySwitch.on {
            eventRecurring[6] = 1
        }
        
        newEvent["recurringDays"] = eventRecurring
        newEvent["endDate"] = endByDate.date
        
        newEvent.saveInBackgroundWithBlock {
            (success: Bool, error: NSError!) -> Void in
            if success && error == nil {
                self.finish.enabled = true
                let alert = UIAlertController(title:"", message:"Your event has been created.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: {(alertAction) -> Void in self.moveOn()}))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else {
                let alert = UIAlertController(title:"", message:"We can't push your event.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    func moveOn() {
        self.performSegueWithIdentifier("toAdmin2", sender: self)
    }
    
    func loadGroupData() {
        var query = PFQuery(className: "Group")
        query.getObjectInBackgroundWithId("\(passedGroupID)") {
            (Group: PFObject!, error: NSError!) -> Void in
            if error == nil && Group != nil {
                self.currentGroup = Group
            }
            else {
                let alert = UIAlertController(title:"", message:"We can't retrieve group information.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again.", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "toAdmin2") {
            let temp = segue.destinationViewController as AdminPage
            temp.passedGroupID = currentGroup.objectId
            temp.passedGroupName = currentGroup["name"] as String
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        groupIDToPass = passedGroupID
        groupNameToPass = passedGroupName
        
        
        loadGroupData()
        
        //changes color of back button to white
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.view.backgroundColor = UIColor.whiteColor()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
